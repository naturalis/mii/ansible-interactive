# ansible-interactive

Ansible role that installs a base for all kinds of interactives: video and audio
players, Unity games, kiosk browsers and more.

## Design

The design of the role is that:

* All interactive applications and related services are running as systemd user
  service.
* Applications that don't require or aren't dependent on graphical Xorg applications
  can be added as systemd user service. These services will autostart based on
  loginctl linger.
* Applications that do require or aren't dependent on graphical Xorg applications
  can be added as systemd user service as well. These services will autostart
  after the systemd system service getty@tty1 starts. Based on the shell profile
  (`~/.zprofile`) and a startup script (`~/start.sh`) the systemd target
  `interactive.target` will start and all related systemd user services as well.

## Requirements

This role is tested on Ubuntu 18.04 (server edition), so you will need to have a
machine running that OS. You need to at least use ansible version 2.7 or higher.
On the destination machine python has to be installed.

## Role Variables

The relevant variables for this role are:

* `gui`. If set to `true`, Xorg, autologin and VNC will be configured.
* `interactive_user`, with subvariables `name`, `id` and `home`. Set these to
   your requirements.
* `vncpassword`. Please change the default password (and use Ansible vault to
   encrypt the secret).
* `video_outputs`. Add a list of video_outputs when you want to mirror and/or
    rotate an output and/or set the resolution/mode. For example:

   ```
   video_outputs:
     - name: DP-1
       rotate: normal
       mode: '1920x1080'
     - name: DP-2
       mirror: DP-1
     - name: DP-3
       flip: horizontal # other options: vertical, both
   ```

* `sound_cards`. Define a list of sound cards based on ALSA name, required
  profile and volume (percentage for all channels or per channel). For example:

  ```
  sound_cards:
    - name: pci-0000_00_1f.3
      profile: hdmi-stereo
      volume: 70%
  ```

* `logwatcher`. For applications with known issues it is possible to add a
   systemd user service that will watch logs for a specified regex on a defined
   interval (in minutes) and in case of a match execute an action. For example:

   ```
   logwatcher:
     interval: 1
     regex: "ArrayTypeMismatchException"
     action: systemctl --user restart unity3d.service
   ```

* `interactive_nextcloud`. If set to `true` a script will be installed to get
  content from a Nextcloud server. The Nextcloud server and credentials are
  configured with these variables:

  ```yaml
  nextcloud_server: https://files.museum.naturalis.nl
  nextcloud_user: ''
  nextcloud_password: ''
  ```

* `interactive_screenshot`. If set to `true` a webserver and script will be
  installed and configured to create and serve screenshots of the display.

## Dependencies

This role depends on `cloudalchemy.node-exporter` for providing basic Prometheus
stats.

## Example Playbook

You can use the interactive role like this:

```yaml
- hosts: all
  gather_facts: false
  vars:
    gui: true
  roles:
    - role: interactive
```


## License

Apache

## Author Information

David Heijkamp working for the museum at Naturalis Biodiversity Center.
